import datetime
import calendar
from datetime import timedelta
from dateutil.relativedelta import relativedelta
import itertools

# take a date or range of dates from the user / fire chief
# parse that date range into individual days

date1 = datetime.datetime(2020, 1, 1)
date2 = datetime.datetime(2020, 4, 30)

# empty array to store the selection range and an iterator variable for the dates
dayRange = []
iDate = date1

# at this point add each individual date to the array
while iDate <= date2:
        dayRange.append(iDate)
        iDate = iDate + timedelta(days=1)

# Crew class defintion
class Crew:
        def __init__(self, title, members, toBeAssigned, lastFireEngineWashed, numFireEnginesWashed):
                self.title = title
                self.members = members
                self.toBeAssigned = toBeAssigned
                self.maxAssigned = len(self.members) - 1
                self.lastFireEngineWashed = lastFireEngineWashed
                self.numFireEnginesWashed = numFireEnginesWashed

# create crew objects from the crew class definition
# should have maybe made individual objects for each crew member and contained attributes within them (jobs completed etc)
CrewOne = Crew(
        "Crew 1",
        [
                "Sam Smith",
                "Barry Manilow",
                "Stephani Germanotta",
                "Richard Nixon",
                "Patrick Bateman",
                "Balthasar Gelt",
                "Chris O'Dowd"
        ],
        0,
        0,
        0
)

CrewTwo = Crew(
        "Crew 2",
        [
                "David Gilmour",
                "Tom Sheldon",
                "Elizabeth Harmon",
                "Shoshanna Dreyfus",
                "John Price",
                "Repanse de Lyonesse",
                "Florence Welch"  
        ],
        0,
        0,
        0
)

# Array of crews
Crews = [
        CrewOne, CrewTwo
]

# iterator for the task id
taskID = itertools.count(0)

# class definition for the tasks
class Task:
        def __init__(self, description, startDate, frequency, crewRequired):
                self.id = next(taskID)
                self.description = description
                self.startDate = startDate
                self.frequency = frequency
                self.crewRequired = crewRequired

# placeholder start date for demonstration
startDate = datetime.datetime(2020, 1, 1)

# create task objects from the template Task class
SweepTheFloors = Task("Sweep the floors", startDate, timedelta(days=1), False)
EmptyTrash = Task("Empty the trash", startDate, timedelta(days=2), False)
WashFireEngine = Task("Wash fire engine", startDate, timedelta(weeks=1), True)
ServiceFireEngine = Task("Service the fire engine", startDate, relativedelta(months=1), True)

# array of tasks
Tasks = [
        SweepTheFloors,
        EmptyTrash,
        WashFireEngine,
        ServiceFireEngine
]

# class for fire engines, simply for an id, descriptor & washed bool
# also create an iterator for the id
fireEngineID = itertools.count(0)

class FE:
        def __init__(self, description, hasBeenWashed):
                self.id = next(fireEngineID)
                self.description = description
                self.hasBeenWashed = hasBeenWashed

FE_1 = FE("1", False)
FE_2 = FE("2", False)

FireEngines = [
        FE_1,
        FE_2
]

# function to update the counters and trackers within the objects, only called twice
def updateObjects(FE, activeCrew):
        FE.hasBeenWashed = True
        activeCrew.lastFireEngineWashed = FE.id
        activeCrew.numFireEnginesWashed += 1

        return FE.description


# initializers,  totalOutput is a culmination of all tasks to be output to file
# serviceWeek is a bool which tracks whether the current week correlates to a service week
# mondayStartDate allows the program to calculate full weeks meaning for easier crew management
totalOutput = ""
serviceWeek = False
mondayStartDate = (startDate - timedelta(days=startDate.weekday()))

# nested loop through both the dayRange array and the Task array
# output the tasks to the console and save to an external.txt

for day in dayRange:
        # similar to mondayStartDate, mondayThisWeek gets the monday of the current week to allow for weekly arithmetic operations to be performed
        # numWeeks is an example of such arithmetic, which is then used to select the activeCrew variable
        mondayThisWeek = (day - timedelta(days=day.weekday()))
        numWeeks = int((mondayThisWeek - mondayStartDate).days / 7) + 1
        activeCrew = Crews[numWeeks % 2]

        # initializes the taskCount variable, only used for output
        taskCount = 1
        # immediately writes the current day/date data to output
        dayOutput = day.strftime("%A %d %b %Y") + ":\n"

        for task in Tasks:
                # initialize the output as empty
                output = ""
                # calculate the difference between the startDate of the scheduling and the current date, convert to days
                difference = (day - task.startDate).days
                # this block only targets task 4 (servicing) task.frequency == 1 month i.e 0 days
                if task.frequency.days == 0:
                        # Service week check must be calculating 1 week and 1 day ahead otherwise service days at the very beginning or end of the month may be missed
                        # using a different calculation for difference here as it benefits from only counting the number of days since the beginning of the month
                        difference = relativedelta(day, task.startDate).days + 1
                        if (difference + 7) == calendar.monthrange(day.year, day.month)[1]:
                                serviceWeek = True

                else:
                        # this block targets all 3 tasks that occur on a >0 days frequency i.e 1-3
                        if (task.frequency != 0 and difference % task.frequency.days == 0):
                                if serviceWeek and task.id == 2:                                               
                                        dependentOutput = "Service the fire engines"
                                        serviceWeek = False
                                else:
                                        dependentOutput = task.description
                                        if task.id == 2 and not(serviceWeek):
                                                
                                                # iterate across the FireEngines array checking each vehicle and comparing it to the value of the last washed FE by the active crew
                                                # assign the active crew to wash the FireEngine which they have not previously washed
                                                for FE in FireEngines:
                                                        if activeCrew.numFireEnginesWashed == 0:
                                                                if not(FE.hasBeenWashed):
                                                                        # calls the updateObjects function and assigns it's return value to the variable output container
                                                                        dependentOutput += " " + str(updateObjects(FE, activeCrew))
                                                                        break
                                                        else:
                                                                if activeCrew.lastFireEngineWashed != FE.id:
                                                                        dependentOutput += " " + str(updateObjects(FE, activeCrew))
                                                                        break
                                
                                # checks if the current task requires an entire crew or just a member and assigns accordingly
                                if task.crewRequired:
                                        assignTo = Crews[numWeeks % 2].title
                                else:
                                        # this block assigns the crew member which is managed by a tracking index stored in the crew object
                                        # the tracking index is then either a) incremented or b) reset to it's initial value to be iterated through again
                                        assignTo = activeCrew.members[activeCrew.toBeAssigned]
                                        activeCrew.toBeAssigned += 1 if activeCrew.toBeAssigned < activeCrew.maxAssigned else -(activeCrew.maxAssigned)

                                # formatting logic
                                tabChars = "\t\t\t" if len(dependentOutput) < 22 else "\t\t"

                                # task output & append to day output
                                output = str(taskCount) + ". " + dependentOutput + tabChars + "Assigned to: " + assignTo + "\n"
                                dayOutput += output
                                # iterate the daily task counter
                                taskCount += 1
        
        # after each day is iterated across, print to the console and add to the final output string
        print(dayOutput)
        totalOutput += dayOutput + "\n"

# write to the external output file
file = open("output.txt", "w")
file.write(totalOutput)
file.close()